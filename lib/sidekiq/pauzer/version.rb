# frozen_string_literal: true

module Sidekiq
  module Pauzer
    VERSION = "4.2.0"
  end
end
